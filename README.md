# Tribal Code Challenge
## Carlos Payan

This a code challenge made for Tribal interview proccess. This project was made with Java Springboot due to the ease of implementation, JPA is used to make the relationship to a MySQL database, and the general design is based on the simplicity of using three main layers of Spring boot that are Controller, Service and Repository 

## Tech

This challenge uses a number of dependencies to work properly:

- [spring-boot] - Java framework
- [mysql-connector] - Creates the bridge between JPA and MySQL
- [lombok] - To make cleaner code
- [jUnit] - Testing framework
- [mockito-core] - For performing the necessary mocks during testing

## Installation

The code challenge requires Java 11+, an instance of MySQL running locally and Maven to run

Install the dependencies and run project

```sh
cd rootFolder
mvn install
```

You could also use and IDE like IntelliJ withour the need of having Maven installed on your machine

## Configuration

To change MySQL properties you can change the values on the file application.properties

```sh
spring.datasource.url = DBUrl
spring.datasource.username = DBUsername
spring.datasource.password = DBPassword
```

## Postman

You can hit the endpoints of this project using this postman collection:
```sh
https://www.getpostman.com/collections/caf55260d1b0abdcb085
```

## Testing

To run tests
```sh
mvn test 
```

   [spring-boot]: <https://spring.io>
   [mysql-connector]: <https://dev.mysql.com/doc/connector-j/8.0/en/connector-j-installing-maven.html>
   [lombok]: <https://spring.io>
   [mockito-core]: <https://projectlombok.org//>
   [jUnit]: <https://junit.org/junit5/t>


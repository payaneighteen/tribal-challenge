package com.tribal.challenge.controller;

import com.tribal.challenge.dto.CreditLineDTO;
import com.tribal.challenge.dto.CreditLineResponseDTO;
import com.tribal.challenge.entity.CreditLine;
import com.tribal.challenge.service.CreditLineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Controller for REST operations of Credit Line
 */
@RestController("creditLine")
@RequestMapping("/creditLine")
public class CreditLineController {

    @Autowired
    CreditLineService creditLineService;

    @GetMapping("/get/{id}")
    public CreditLine get(@PathVariable long id) {
        return creditLineService.getCreditLine(id);
    }

    @PostMapping("/customer/{id}/apply")
    public CreditLineResponseDTO submit(@PathVariable long id, @Valid @RequestBody CreditLineDTO creditLineDTO) {
        return creditLineService.createCreditLine(id, creditLineDTO);
    }

}

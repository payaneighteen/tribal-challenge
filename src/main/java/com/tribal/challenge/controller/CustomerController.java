package com.tribal.challenge.controller;

import com.tribal.challenge.dto.CustomerDTO;
import com.tribal.challenge.entity.Customer;
import com.tribal.challenge.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Controller for REST operations of Customer
 */
@RestController("customer")
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @GetMapping("/get/{id}")
    public Customer get(@PathVariable long id) {
        return customerService.getCustomer(id);
    }

    @PostMapping("/create")
    public Customer submit(@Valid @RequestBody CustomerDTO customerDTO) {
        return customerService.createCustomer(customerDTO);
    }

}

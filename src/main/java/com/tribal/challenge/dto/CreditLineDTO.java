package com.tribal.challenge.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.tribal.challenge.entity.BusinessType;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Post DTO for credit line
 */
@Data
public class CreditLineDTO {
    @NotNull
    private BigDecimal cashBalance;

    @NotNull
    private BusinessType businessType;

    @NotNull
    private BigDecimal monthlyRevenue;

    @NotNull
    private BigDecimal requestedCreditLine;

    private Date requestedDate;

    public Date getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(Date requestedDate) {
        this.requestedDate = requestedDate;
    }

    @JsonCreator
    public CreditLineDTO(BigDecimal cashBalance, BusinessType businessType, BigDecimal monthlyRevenue, BigDecimal requestedCreditLine, Date requestedDate) {
        this.cashBalance = cashBalance;
        this.businessType = businessType;
        this.monthlyRevenue = monthlyRevenue;
        this.requestedCreditLine = requestedCreditLine;
        this.requestedDate = requestedDate;
    }
}

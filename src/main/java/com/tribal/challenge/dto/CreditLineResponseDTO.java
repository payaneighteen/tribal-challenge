package com.tribal.challenge.dto;

import com.tribal.challenge.entity.CreditLine;
import lombok.Data;

/**
 * Credit Line response DTO for POST
 */
@Data
public class CreditLineResponseDTO {
    private CreditLine creditLine;

    private String message;

    public CreditLineResponseDTO(){}

    public CreditLineResponseDTO(CreditLine creditLine, String message) {
        this.creditLine = creditLine;
        this.message = message;
    }
}

package com.tribal.challenge.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * Post DTO for customer
 */
@Data
public class CustomerDTO {

    @NotBlank
    private String name;

    @JsonCreator
    public CustomerDTO(String name) {
        this.name = name;
    }
}

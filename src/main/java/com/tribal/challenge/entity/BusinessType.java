package com.tribal.challenge.entity;


/**
 * Represents the business types in the system.
 */
public enum BusinessType{
    STARTUP,
    SME
}

package com.tribal.challenge.entity;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Represents a credit line in the system.
 */

@Entity
@Table(name = "credit_line")
@EntityListeners(AuditingEntityListener.class)
@Builder
@Data
@NoArgsConstructor
public class CreditLine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = {
            CascadeType.ALL
    })
    private Customer customer;

    private BigDecimal cashBalance;

    private BusinessType businessType;

    private BigDecimal monthlyRevenue;

    private BigDecimal creditLine;

    private Date requestedDate;

    private boolean approved;

    public CreditLine(long id, Customer customer, BigDecimal cashBalance, BusinessType businessType, BigDecimal monthlyRevenue, BigDecimal creditLine, Date requestedDate, boolean approved) {
        this.id = id;
        this.customer = customer;
        this.cashBalance = cashBalance;
        this.businessType = businessType;
        this.monthlyRevenue = monthlyRevenue;
        this.creditLine = creditLine;
        this.requestedDate = requestedDate;
        this.approved = approved;
    }
}


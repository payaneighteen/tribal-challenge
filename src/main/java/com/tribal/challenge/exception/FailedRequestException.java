package com.tribal.challenge.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class FailedRequestException extends RuntimeException{
    public FailedRequestException() {
        super("A sales agent will contact you");
    }
}

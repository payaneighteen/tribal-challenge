package com.tribal.challenge.repository;


import com.tribal.challenge.entity.CreditLine;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for all CRUD operations of Credit Line
 */
@Repository
public interface CreditLineRepository extends CrudRepository<CreditLine, Long> {
    List<CreditLine> findByCustomer_Id(Long customerId);
}

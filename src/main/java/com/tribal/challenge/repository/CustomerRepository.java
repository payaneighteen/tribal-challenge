package com.tribal.challenge.repository;


import com.tribal.challenge.entity.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for all CRUD operations of Customer
 */
@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {

}

package com.tribal.challenge.service;


import com.tribal.challenge.dto.CreditLineDTO;
import com.tribal.challenge.dto.CreditLineResponseDTO;
import com.tribal.challenge.entity.BusinessType;
import com.tribal.challenge.entity.CreditLine;
import com.tribal.challenge.entity.Customer;
import com.tribal.challenge.exception.FailedRequestException;
import com.tribal.challenge.exception.ResourceNotFoundException;
import com.tribal.challenge.exception.TooManyRequestsException;
import com.tribal.challenge.repository.CreditLineRepository;
import com.tribal.challenge.repository.CustomerRepository;
import com.tribal.challenge.util.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;


/**
 * Service implementation for credit line.
 */
@Service
public class CreditLineService {

    @Autowired
    CreditLineRepository creditLineRepository;

    @Autowired
    CustomerRepository customerRepository;

    /**
     * Creates a new credit line application on the system
     */
    public CreditLineResponseDTO createCreditLine(Long customerId, CreditLineDTO creditLineDTO) {
        creditLineDTO.setRequestedDate(new Date());

        Customer customer = customerRepository.findById(customerId)
                .orElseThrow(() -> new ResourceNotFoundException("Customer", "id", customerId));

        List<CreditLine> creditLines = creditLineRepository.findByCustomer_Id(customerId);

        CreditLineResponseDTO creditLineResponseDTO = new CreditLineResponseDTO();

        if (creditLines.size() > 0) {
            CreditLine previousCreditLine = validatePreviousApplications(creditLines, creditLineDTO);
            if (previousCreditLine != null) {

                CreditLine logCreditLine = new CreditLine().builder()
                        .customer(previousCreditLine.getCustomer())
                        .cashBalance(previousCreditLine.getCashBalance())
                        .businessType(previousCreditLine.getBusinessType())
                        .monthlyRevenue(previousCreditLine.getMonthlyRevenue())
                        .creditLine(previousCreditLine.getCreditLine())
                        .requestedDate(creditLineDTO.getRequestedDate())
                        .approved(true)
                        .build();

                return new CreditLineResponseDTO(creditLineRepository.save(logCreditLine), "A previous application was already accepted.");
            }
        }

        BigDecimal recommendedCredit = recommendedCreditCalc(creditLineDTO);

        CreditLine creditLine = new CreditLine().builder()
                .customer(customer)
                .cashBalance(creditLineDTO.getCashBalance())
                .businessType(creditLineDTO.getBusinessType())
                .monthlyRevenue(creditLineDTO.getMonthlyRevenue())
                .creditLine(creditLineDTO.getRequestedCreditLine())
                .requestedDate(creditLineDTO.getRequestedDate())
                .build();

        if (recommendedCredit.compareTo(creditLineDTO.getRequestedCreditLine()) > 0) {
            creditLine.setApproved(true);
            creditLineResponseDTO.setMessage("Your application was accepted.");
        } else {
            creditLine.setApproved(false);
            creditLineResponseDTO.setMessage("Your application was rejected.");
        }
        creditLineResponseDTO.setCreditLine(creditLineRepository.save(creditLine));

        return creditLineResponseDTO;
    }

    /**
     * Gets credit line by id
     */
    public CreditLine getCreditLine(long id) {
        return creditLineRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Credit Line", "id", id));
    }

    /**
     * Checks all previous credit lines applications by customer
     */
    public CreditLine validatePreviousApplications(List<CreditLine> creditLines, CreditLineDTO creditLineDTO) {
        if (creditLines.stream().anyMatch(CreditLine::isApproved)) {
            if (creditLines.stream().filter(creditLine -> creditLine.isApproved()
                    && TimeUtil.check120Seconds(creditLine.getRequestedDate(), creditLineDTO.getRequestedDate())).count() >= 3) {
                throw new TooManyRequestsException();
            }
            //Assign same accepted credit regardless of the new inputs
            return creditLines.stream().filter(CreditLine::isApproved).findFirst().get();
        } else {
            if (creditLines.stream().filter(creditLine -> !creditLine.isApproved()).count() >= 3) {
                throw new FailedRequestException();
            } else if (creditLines.stream().anyMatch(creditLine -> !creditLine.isApproved()
                    && TimeUtil.check30Seconds(creditLine.getRequestedDate(), creditLineDTO.getRequestedDate()))) {
                throw new TooManyRequestsException();
            }
            return null;
        }
    }

    /**
     * Returns the recommend credit line by business type
     */
    public BigDecimal recommendedCreditCalc(CreditLineDTO creditLineDTO) {
        if (creditLineDTO.getBusinessType().equals(BusinessType.SME)) {
            return creditLineDTO.getMonthlyRevenue().divide(new BigDecimal(5), 2, RoundingMode.HALF_EVEN);
        } else {
            BigDecimal cashBalanceRatio = creditLineDTO.getCashBalance().divide(new BigDecimal(3), 2, RoundingMode.HALF_EVEN);
            BigDecimal monthlyRevenueRatio = creditLineDTO.getMonthlyRevenue().divide(new BigDecimal(5), 2, RoundingMode.HALF_EVEN);

            if (cashBalanceRatio.compareTo(monthlyRevenueRatio) > 0) {
                return cashBalanceRatio;
            } else {
                return monthlyRevenueRatio;
            }
        }
    }
}

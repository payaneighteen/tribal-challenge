package com.tribal.challenge.service;


import com.tribal.challenge.dto.CustomerDTO;
import com.tribal.challenge.entity.Customer;
import com.tribal.challenge.exception.ResourceNotFoundException;
import com.tribal.challenge.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service implementation for customer.
 */
@Service
public class CustomerService{

    @Autowired
    CustomerRepository customerRepository;

    /**
     * Creates a new customer in the system
     */
    public Customer createCustomer(CustomerDTO customerDTO) {
        return customerRepository.save(new Customer(customerDTO.getName()));
    }

    /**
     * Gets customer by id
     */
    public Customer getCustomer(long id) {
        return customerRepository.findById(id)
        .orElseThrow(() -> new ResourceNotFoundException("Customer", "id", id));
    }

}

package com.tribal.challenge.util;

import java.util.Date;

public final class TimeUtil {
    /**
     * Function to determine 30 seconds between two dates
     * @param previousApplication
     * @param currentApplication
     * @return
     */
    public static boolean check30Seconds(Date previousApplication, Date currentApplication){
        return (currentApplication.getTime() - previousApplication.getTime()) / 1000 <= 30;
    }

    /**
     * Function to determine 120 seconds between two dates
     * @param previousApplication
     * @param currentApplication
     * @return
     */
    public static boolean check120Seconds(Date previousApplication, Date currentApplication){
        return (currentApplication.getTime() - previousApplication.getTime()) / 1000 <= 120;
    }
}


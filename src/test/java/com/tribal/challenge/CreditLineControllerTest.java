package com.tribal.challenge;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tribal.challenge.dto.CreditLineDTO;
import com.tribal.challenge.entity.BusinessType;
import com.tribal.challenge.entity.CreditLine;
import com.tribal.challenge.entity.Customer;
import com.tribal.challenge.repository.CreditLineRepository;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class CreditLineControllerTest {

	@Autowired
	MockMvc mockMvc;

	@Autowired
	WebApplicationContext webApplicationContext;

	@MockBean
	CreditLineRepository creditLineRepository;

	@Autowired
	ObjectMapper mapper;

	Customer customer = new Customer(1L, "Carlos Payan");
	CreditLine RECORD_1 = new CreditLine(1L,customer, new BigDecimal(100), BusinessType.SME, new BigDecimal(100), new BigDecimal(100), new Date(), true);

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void getCreditLine_success() throws Exception {
		Mockito.when(creditLineRepository.findById(RECORD_1.getId())).thenReturn(java.util.Optional.of(RECORD_1));

		mockMvc.perform(MockMvcRequestBuilders
				.get("/creditLine/get/1")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", notNullValue()))
				.andExpect(jsonPath("$.creditLine").value(RECORD_1.getCreditLine()));
	}

	@Test
	public void applyCreditLine_SME_rejected() throws Exception {
		Customer customer = new Customer(2L, "Alejandro Castellanos");
		CreditLine creditLine = new CreditLine(2L,customer, new BigDecimal(100), BusinessType.SME, new BigDecimal(100), new BigDecimal(100), new Date(), false);
		CreditLineDTO creditLineDTO = new CreditLineDTO(new BigDecimal(100), BusinessType.SME, new BigDecimal(100), new BigDecimal(100), new Date());


		Mockito.when(creditLineRepository.save(creditLine)).thenReturn(creditLine);

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/creditLine/customer/2/apply")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(creditLineDTO));

		mockMvc.perform(mockRequest)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", notNullValue()))
				.andExpect(jsonPath("$.message").value("Your application was rejected."));
	}

	@Test
	public void applyCreditLine_SME_success() throws Exception {
		Customer customer = new Customer(2L, "Alejandro Castellanos");
		CreditLine creditLine = new CreditLine(2L,customer, new BigDecimal("435.30"), BusinessType.SME, new BigDecimal("4235.45"), new BigDecimal(500), new Date(), true);
		CreditLineDTO creditLineDTO = new CreditLineDTO(new BigDecimal("435.30"), BusinessType.SME, new BigDecimal("4235.45"), new BigDecimal(500), new Date());


		Mockito.when(creditLineRepository.save(creditLine)).thenReturn(creditLine);

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/creditLine/customer/2/apply")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(creditLineDTO));

		mockMvc.perform(mockRequest)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", notNullValue()))
				.andExpect(jsonPath("$.message").value("Your application was accepted."));
	}

	@Test
	public void applyCreditLine_STARTUP_rejected() throws Exception {
		Customer customer = new Customer(2L, "Alejandro Castellanos");
		CreditLine creditLine = new CreditLine(2L,customer, new BigDecimal(200), BusinessType.STARTUP, new BigDecimal(450), new BigDecimal(100), new Date(), false);
		CreditLineDTO creditLineDTO = new CreditLineDTO(new BigDecimal(200), BusinessType.STARTUP, new BigDecimal(450), new BigDecimal(100), new Date());


		Mockito.when(creditLineRepository.save(creditLine)).thenReturn(creditLine);

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/creditLine/customer/2/apply")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(creditLineDTO));

		mockMvc.perform(mockRequest)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", notNullValue()))
				.andExpect(jsonPath("$.message").value("Your application was rejected."));
	}

	@Test
	public void applyCreditLine_STARTUP_success() throws Exception {
		Customer customer = new Customer(2L, "Alejandro Castellanos");
		CreditLine creditLine = new CreditLine(2L,customer, new BigDecimal("5231.66"), BusinessType.STARTUP, new BigDecimal("4235.45"), new BigDecimal(920), new Date(), true);
		CreditLineDTO creditLineDTO = new CreditLineDTO(new BigDecimal("5231.66"), BusinessType.STARTUP, new BigDecimal("4235.45"), new BigDecimal(920), new Date());


		Mockito.when(creditLineRepository.save(creditLine)).thenReturn(creditLine);

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/creditLine/customer/2/apply")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(creditLineDTO));

		mockMvc.perform(mockRequest)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", notNullValue()))
				.andExpect(jsonPath("$.message").value("Your application was accepted."));
	}

	@Test
	public void applyCreditLine_success_same_credit_second_attempt() throws Exception {
		Customer customer = new Customer(2L, "Alejandro Castellanos");
		CreditLine creditLine = new CreditLine(2L,customer, new BigDecimal("435.30"), BusinessType.SME, new BigDecimal("4235.45"), new BigDecimal(500), new Date(), true);
		CreditLineDTO creditLineDTO = new CreditLineDTO(new BigDecimal("435.30"), BusinessType.SME, new BigDecimal("4235.45"), new BigDecimal(500), new Date());


		CreditLine creditLine_RECORD_1 = new CreditLine(2L,customer, new BigDecimal("435.30"), BusinessType.SME, new BigDecimal("4235.45"), new BigDecimal(500), new Date(), true);

		List<CreditLine> creditLines = new ArrayList<>();

		creditLines.add(creditLine_RECORD_1);

		Mockito.when(creditLineRepository.save(creditLine)).thenReturn(creditLine);
		Mockito.when(creditLineRepository.findByCustomer_Id(customer.getId())).thenReturn(creditLines);

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/creditLine/customer/2/apply")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(creditLineDTO));

		mockMvc.perform(mockRequest)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", notNullValue()))
				.andExpect(jsonPath("$.message").value("A previous application was already accepted."));
	}

	@Test
	public void applyCreditLine_success_third_time_too_many_requests() throws Exception {
		Customer customer = new Customer(2L, "Alejandro Castellanos");
		CreditLine creditLine = new CreditLine(2L,customer, new BigDecimal("435.30"), BusinessType.SME, new BigDecimal("4235.45"), new BigDecimal(500), new Date(), true);
		CreditLineDTO creditLineDTO = new CreditLineDTO(new BigDecimal("435.30"), BusinessType.SME, new BigDecimal("4235.45"), new BigDecimal(500), new Date());

		CreditLine creditLine_RECORD_1 = new CreditLine(2L,customer, new BigDecimal("435.30"), BusinessType.SME, new BigDecimal("4235.45"), new BigDecimal(500), new Date(), true);
		CreditLine creditLine_RECORD_2 = new CreditLine(2L,customer, new BigDecimal("435.30"), BusinessType.SME, new BigDecimal("4235.45"), new BigDecimal(500), new Date(), true);
		CreditLine creditLine_RECORD_3 = new CreditLine(2L,customer, new BigDecimal("435.30"), BusinessType.SME, new BigDecimal("4235.45"), new BigDecimal(500), new Date(), true);

		List<CreditLine> creditLines = new ArrayList<>();

		creditLines.add(creditLine_RECORD_1);
		creditLines.add(creditLine_RECORD_2);
		creditLines.add(creditLine_RECORD_3);

		Mockito.when(creditLineRepository.save(creditLine)).thenReturn(creditLine);
		Mockito.when(creditLineRepository.findByCustomer_Id(customer.getId())).thenReturn(creditLines);

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/creditLine/customer/2/apply")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(creditLineDTO));

		mockMvc.perform(mockRequest)
				.andExpect(status().isTooManyRequests());
	}

	@Test
	public void applyCreditLine_rejected_too_many_requests() throws Exception {
		Customer customer = new Customer(2L, "Alejandro Castellanos");
		CreditLine creditLine = new CreditLine(2L,customer, new BigDecimal(100), BusinessType.SME, new BigDecimal(100), new BigDecimal(100), new Date(), false);
		CreditLineDTO creditLineDTO = new CreditLineDTO(new BigDecimal(100), BusinessType.SME, new BigDecimal(100), new BigDecimal(100), new Date());

		CreditLine creditLine_RECORD_1 = new CreditLine(2L,customer, new BigDecimal("435.30"), BusinessType.SME, new BigDecimal("4235.45"), new BigDecimal(500), new Date(), false);

		List<CreditLine> creditLines = new ArrayList<>();

		creditLines.add(creditLine_RECORD_1);

		Mockito.when(creditLineRepository.save(creditLine)).thenReturn(creditLine);
		Mockito.when(creditLineRepository.findByCustomer_Id(customer.getId())).thenReturn(creditLines);

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/creditLine/customer/2/apply")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(creditLineDTO));

		mockMvc.perform(mockRequest)
				.andExpect(status().isTooManyRequests());
	}

	@Test
	public void applyCreditLine_rejected_failed_request() throws Exception {
		Customer customer = new Customer(2L, "Alejandro Castellanos");
		CreditLine creditLine = new CreditLine(2L,customer, new BigDecimal(100), BusinessType.SME, new BigDecimal(100), new BigDecimal(100), new Date(), false);
		CreditLineDTO creditLineDTO = new CreditLineDTO(new BigDecimal(100), BusinessType.SME, new BigDecimal(100), new BigDecimal(100), new Date());

		CreditLine creditLine_RECORD_1 = new CreditLine(2L,customer, new BigDecimal("435.30"), BusinessType.SME, new BigDecimal("4235.45"), new BigDecimal(500), new Date(), false);
		CreditLine creditLine_RECORD_2 = new CreditLine(2L,customer, new BigDecimal("435.30"), BusinessType.SME, new BigDecimal("4235.45"), new BigDecimal(499), new Date(), false);
		CreditLine creditLine_RECORD_3 = new CreditLine(2L,customer, new BigDecimal("435.30"), BusinessType.SME, new BigDecimal("4235.45"), new BigDecimal(498), new Date(), false);

		List<CreditLine> creditLines = new ArrayList<>();

		creditLines.add(creditLine_RECORD_1);
		creditLines.add(creditLine_RECORD_2);
		creditLines.add(creditLine_RECORD_3);

		Mockito.when(creditLineRepository.save(creditLine)).thenReturn(creditLine);
		Mockito.when(creditLineRepository.findByCustomer_Id(customer.getId())).thenReturn(creditLines);

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/creditLine/customer/2/apply")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(creditLineDTO));

		mockMvc.perform(mockRequest)
				.andExpect(status().isConflict());
	}
}

package com.tribal.challenge;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tribal.challenge.dto.CustomerDTO;
import com.tribal.challenge.entity.Customer;
import com.tribal.challenge.repository.CustomerRepository;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class CustomerControllerTest {

	@Autowired
	MockMvc mockMvc;

	@Autowired
	WebApplicationContext webApplicationContext;

	@MockBean
	CustomerRepository customerRepository;

	@Autowired
	ObjectMapper mapper;

	Customer RECORD_1 = new Customer(1L, "Carlos Payán");

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void getCustomer_success() throws Exception {
		Mockito.when(customerRepository.findById(RECORD_1.getId())).thenReturn(java.util.Optional.of(RECORD_1));

		mockMvc.perform(MockMvcRequestBuilders
				.get("/customer/get/1")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", notNullValue()))
				.andExpect(jsonPath("$.name").value(RECORD_1.getName()));
	}

	@Test
	public void createCustomer_success() throws Exception {
		Customer customer = new Customer(3L, "Alejandro Castellanos");
		CustomerDTO customerDTO = new CustomerDTO( "Alejandro Castellanos");

		Mockito.when(customerRepository.save(customer)).thenReturn(customer);

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/customer/create")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(customerDTO));

		mockMvc.perform(mockRequest)
				.andExpect(status().isOk());
	}

}
